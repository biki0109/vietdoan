package services

import (
	"context"

	"git.begroup.team/platform-transport/vietdoan/config"
	"git.begroup.team/platform-transport/vietdoan/internal/stores"
	"git.begroup.team/platform-transport/vietdoan/pb"
)

const Version = "1.0.0"

type service struct {
	isReady   bool
	cfg       *config.Config
	mainStore *stores.MainStore
}

func New(config *config.Config,
	mainStore *stores.MainStore) pb.VietdoanServer {
	return &service{
		isReady:   true,
		cfg:       config,
		mainStore: mainStore,
	}
}

func (s *service) Version(context context.Context, req *pb.VersionRequest) (*pb.VersionResponse, error) {
	return &pb.VersionResponse{
		Version: Version,
	}, nil
}
