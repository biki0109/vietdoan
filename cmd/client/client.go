package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/vietdoan/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type VietdoanClient struct {
	pb.VietdoanClient
}

func NewVietdoanClient(address string) *VietdoanClient {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)
	// TODO client ne
	if err != nil {
		ll.Fatal("Failed to dial Vietdoan service", l.Error(err))
	}

	c := pb.NewVietdoanClient(conn)

	return &VietdoanClient{c}
}
