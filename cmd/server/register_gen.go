// IM AUTO GENERATED, BUT CAN BE OVERRIDDEN

package main

import (
	"git.begroup.team/platform-transport/vietdoan/config"
	"git.begroup.team/platform-transport/vietdoan/internal/services"
	"git.begroup.team/platform-transport/vietdoan/internal/stores"
	"git.begroup.team/platform-transport/vietdoan/pb"
)

func registerService(cfg *config.Config) pb.VietdoanServer {

	mainStore := stores.NewMainStore()

	return services.New(cfg, mainStore)
}
